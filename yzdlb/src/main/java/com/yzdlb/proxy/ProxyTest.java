package com.yzdlb.proxy;

public class ProxyTest {
	public static void main(String[] args) {
		UserService userService = new UserServiceImpl();
		MyInvocationHandler invocationHandler = new MyInvocationHandler(userService);
		UserService proxy = (UserService) invocationHandler.proxy();
		proxy.add();
	}
}
