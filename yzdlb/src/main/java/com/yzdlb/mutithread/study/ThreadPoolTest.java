package com.yzdlb.mutithread.study;

import com.yzdlb.mutithread.study.ThreadPoolManager.RunnableTask;

/***************************************************************************
 * <pre></pre>
 * 
 * @ClassName: ThreadPoolTest.java
 * @PackagePath: com.yzdlb.mutithread.study
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:
 * @Author: yangzhendong
 * @Date: 2016年8月23日 下午2:14:28
 ***************************************************************************/
public class ThreadPoolTest {
    public static void main(String[] args) {
        final RunnableTask task = new RunnableTask() {
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "...is execute...");
            }
        };
        ThreadPoolManager poolManager = new ThreadPoolManager();
        for (int i = 0; i < 10; i++) {
            poolManager.execute(task);
        }
    }
}
