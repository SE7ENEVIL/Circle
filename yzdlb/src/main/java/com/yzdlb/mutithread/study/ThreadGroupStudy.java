package com.yzdlb.mutithread.study;

/***************************************************************************
 * <pre></pre>
 * 
 * @ClassName: ThreadGroup.java
 * @PackagePath: com.yzdlb.mutithread.study
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description: 线程组练习
 * @Author: yangzhendong
 * @Date: 2016年8月23日 下午1:14:45
 ***************************************************************************/
public class ThreadGroupStudy {
    public static void main(String[] args) {
        ThreadGroup tg = new ThreadGroup("tg1");
        Thread t1 = new Thread(tg, "线程1") {
            @Override
            public void run() {
                System.out.println("t1...." + getThreadGroup().getName());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t2 = new Thread(tg, "线程2") {
            @Override
            public void run() {
                while (true) {
                    System.out.println("t2....");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread t3 = new Thread(tg, "线程3") {
            @Override
            public void run() {
                while (true) {
                    System.out.println("t3....");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t1.start();
        System.out.println("Active Count:" + tg.activeCount());
        t2.start();
        System.out.println("Active Count:" + tg.activeCount());
        t3.start();
        System.out.println("Active Count:" + tg.activeCount());
        tg.list();

    }
}
