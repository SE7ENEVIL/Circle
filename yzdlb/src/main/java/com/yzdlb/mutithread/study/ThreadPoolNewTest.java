package com.yzdlb.mutithread.study;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/***************************************************************************
 * <pre></pre>
 * 
 * @ClassName: ThreadPoolNewTest.java
 * @PackagePath: com.yzdlb.mutithread.study
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:
 * @Author: yangzhendong
 * @Date: 2016年8月24日 下午6:29:45
 ***************************************************************************/
public class ThreadPoolNewTest {

    private ThreadPool pool = null;

    @BeforeTest
    public void init() {
        pool = new ThreadPool(10, 15, 30); // default value
    }

    @AfterTest
    public void release() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pool.destory();
        pool = null;
    }

    @Test
    public void testnormal() {
        for (int i = 0; i < 5; i++) {
            TestTask tt = new TestTask();
            pool.execute(tt);
        }
    }

    private static class TestTask implements ThreadPool.Task {
        public void run() {
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) { // TODO Auto-generated catch
                                               // block
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName());
        }
    }
}
