package com.yzdlb.security;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/***************************************************************************
 * <pre></pre>
 * @ClassName:  CryptoPackageTest.java
 * @PackagePath:  com.yzdlb.security
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:  crypto包测试
 * @Author: yangzhendong
 * @Date: 2016年5月23日 下午3:57:55
 ***************************************************************************/
public class CryptoPackageTest {
    
    public void testKeyGenerator() throws Exception{
        KeyGenerator keyGenerator = KeyGenerator.getInstance("HmacMD5");
        SecretKey secretKey = keyGenerator.generateKey();
    }
    
    public void testKeyAgreement() throws Exception{
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("DH");
        KeyPair kp1 = kpg.genKeyPair();
        KeyPair kp2 = kpg.genKeyPair();
        
        
        
    }
    
    
    public static void main(String[] args) throws NoSuchAlgorithmException {
        
    }
    
    
    
}
