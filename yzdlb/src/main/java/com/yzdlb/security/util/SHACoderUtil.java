package com.yzdlb.security.util;

import java.security.MessageDigest;

/***************************************************************************
 * <pre></pre>
 * @ClassName:  SHACoderUtil.java
 * @PackagePath:  com.yzdlb.security.util
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:  
 * @Author: yangzhendong
 * @Date: 2016年6月1日 下午5:30:36
 ***************************************************************************/
public class SHACoderUtil {
    public static byte[] encodeSHA(byte[] data) throws Exception{
        MessageDigest md = MessageDigest.getInstance("SHA");
        return md.digest(data);
    }
    
    public static byte[] encodeSHA256(byte[] data) throws Exception{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(data);
    }
    
    public static byte[] encodeSHA384(byte[] data) throws Exception{
        MessageDigest md = MessageDigest.getInstance("SHA-384");
        return md.digest(data);
    }
    public static byte[] encodeSHA512(byte[] data) throws Exception{
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        return md.digest(data);
    }
}
