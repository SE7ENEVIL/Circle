package com.yzdlb.security.util;

import java.security.MessageDigest;

/***************************************************************************
 * <pre></pre>
 * 
 * @ClassName: MDCodeUtil.java
 * @PackagePath: com.yzdlb.security.util
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:
 * @Author: yangzhendong
 * @Date: 2016年5月31日 下午3:13:13
 ***************************************************************************/
public abstract class MDCoderUtil {
    public static byte[] encodeMD2(byte[] data) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD2");
        return md.digest(data);
    }
    
    public static byte[] encodeMD5(byte[] data) throws Exception{
        MessageDigest md = MessageDigest.getInstance("MD5");
        return md.digest(data);
    }
}
