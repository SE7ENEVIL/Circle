package com.yzdlb.security.util;
/***************************************************************************
 * <pre></pre>
 * @ClassName:  AESCoder.java
 * @PackagePath:  com.yzdlb.security.util
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:  
 * @Author: yangzhendong
 * @Date: 2016年6月23日 下午2:09:00
 ***************************************************************************/
public class AESCoder {
    public static final String KEY_ALGORITHM = "AES";
    
    public static final String CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding" ;
}
