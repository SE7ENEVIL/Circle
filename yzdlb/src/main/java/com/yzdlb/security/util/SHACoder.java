package com.yzdlb.security.util;

import org.apache.commons.codec.digest.DigestUtils;

/***************************************************************************
 * <pre></pre>
 * 
 * @ClassName: SHACoder.java
 * @PackagePath: com.yzdlb.security.util
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:
 * @Author: yangzhendong
 * @Date: 2016年6月3日 上午9:58:03
 ***************************************************************************/
public abstract class SHACoder {

    public static byte[] encodeSHA(String data) throws Exception {
        return DigestUtils.sha1(data);
    }

    public static String encodeSHAHex(String data) throws Exception {
        return DigestUtils.sha1Hex(data);
    }

    public static byte[] encodeSHA256(String data) throws Exception {
        return DigestUtils.sha256(data);
    }

    public static String encodeSHA256Hex(String data) throws Exception {
        return DigestUtils.sha256Hex(data);
    }

    public static byte[] encodeSHA384(String data) throws Exception {
        return DigestUtils.sha384(data);
    }

    public static String encodeSHA384Hex(String data) throws Exception {
        return DigestUtils.sha384Hex(data);
    }

    public static byte[] encodeSHA512(String data) throws Exception {
        return DigestUtils.sha512(data);
    }

    public static String encodeSHA512Hex(String data) throws Exception {
        return DigestUtils.sha512Hex(data);
    }

}
