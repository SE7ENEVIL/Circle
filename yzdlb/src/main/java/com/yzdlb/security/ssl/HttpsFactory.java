package com.yzdlb.security.ssl;


import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


/***************************************************************************
 * <pre></pre>
 * @ClassName:  HttpsFactory.java
 * @PackagePath:  com.yzdlb.security.ssl
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:  
 * @Author: yangzhendong
 * @Date: 2016年5月25日 上午11:29:47
 ***************************************************************************/
public class HttpsFactory {
    public static void main(String[] args) throws Exception {
        // 构建URL对象
        URL url = new URL("https://www.sun.com/");
        //获得HttpsURLConnection 实例化对象
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        //打开输入模式
        conn.setDoInput(true);
        //打开输出模式
        conn.setDoOutput(true);
        
        
    }
    
    
}
