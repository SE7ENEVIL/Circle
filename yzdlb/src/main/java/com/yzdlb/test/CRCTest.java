package com.yzdlb.test;

import java.util.zip.CRC32;

import org.testng.annotations.Test;

/***************************************************************************
 * <pre></pre>
 * 
 * @ClassName: CRCTest.java
 * @PackagePath: com.yzdlb.security.util
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:
 * @Author: yangzhendong
 * @Date: 2016年6月3日 上午11:09:59
 ***************************************************************************/
public class CRCTest {
    @Test
    public void testCRC32() throws Exception {
        String str = "测试CRC-32";
        CRC32 crc32 = new CRC32();
        crc32.update(str.getBytes());
        String hex = Long.toHexString(crc32.getValue());
        System.err.println("原文:  " + str);
        System.err.println("CRC-32: " + hex);

    }
}
