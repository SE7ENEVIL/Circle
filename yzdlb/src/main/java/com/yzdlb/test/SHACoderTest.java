package com.yzdlb.test;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

import com.yzdlb.security.util.SHACoder;
import com.yzdlb.security.util.SHACoderUtil;
/***************************************************************************
 * <pre></pre>
 * @ClassName:  SHACoderTest.java
 * @PackagePath:  com.yzdlb.test
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:  
 * @Author: yangzhendong
 * @Date: 2016年6月2日 下午4:37:31
 ***************************************************************************/
public class SHACoderTest {

    @Test
    public final void testEncodeSHA() throws Exception{
        String str = "SHA1消息摘要";
        byte[] data1 = SHACoderUtil.encodeSHA(str.getBytes());
        byte[] data2 = SHACoderUtil.encodeSHA(str.getBytes());
        assertEquals(data1, data2);
    }
    @Test
    public final void testEncodeSHA256() throws Exception{
        String str = "SHA256消息摘要";
        
        byte[] data1 = SHACoderUtil.encodeSHA256(str.getBytes());
        byte[] data2 = SHACoderUtil.encodeSHA256(str.getBytes());
        assertEquals(data1, data2);
    }   
    @Test
    public final void testEncodeSHA384() throws Exception{
        String str = "SHA384消息摘要";
        
        byte[] data1 = SHACoderUtil.encodeSHA384(str.getBytes());
        byte[] data2 = SHACoderUtil.encodeSHA384(str.getBytes());
        assertEquals(data1, data2);
    }
    @Test
    public final void testEncodeSHA512() throws Exception{
        String str = "SHA512消息摘要";
        
        byte[] data1 = SHACoderUtil.encodeSHA512(str.getBytes());
        byte[] data2 = SHACoderUtil.encodeSHA512(str.getBytes());
        assertEquals(data1, data2);
    }
    @Test
    public final void testEncodeSHAHex()throws Exception{
        String str = "SHA-1Hex消息摘要";
        String data1 = SHACoder.encodeSHAHex(str);
        String data2 = SHACoder.encodeSHAHex(str);
        System.err.println("原文: \t" + str);
        System.err.println("SHAHex-1: \t" + data1);
        System.err.println("SHAHex-2: \t" + data2);
        assertEquals(data1,data2);
    }
}
