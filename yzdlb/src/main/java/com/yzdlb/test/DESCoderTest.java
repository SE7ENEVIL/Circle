package com.yzdlb.test;

import org.apache.commons.codec.binary.Base64;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

import com.yzdlb.security.util.DESCoder;

/***************************************************************************
 * <pre></pre>
 * @ClassName:  DESCoderTest.java
 * @PackagePath:  com.yzdlb.test
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:  
 * @Author: yangzhendong
 * @Date: 2016年6月16日 下午5:25:06
 ***************************************************************************/
public class DESCoderTest {
    @Test
    public final void test() throws Exception{
        String inputStr = "DES你好啊啊啊啊啊";
        byte[] inputData = inputStr.getBytes();
        System.err.println("原文:\t" + inputStr);
        
        //初始化密钥
        byte[] key = DESCoder.initKey();
        System.err.println("密钥:\t" + Base64.encodeBase64String(key));
        
        //加密
        inputData = DESCoder.encrypt(inputData, key);
        System.err.println("加密后:\t" + Base64.encodeBase64String(inputData));
        
        //解密
        byte[] outputData = DESCoder.decrypt(inputData, key);
        String outputStr = new String(outputData);
        System.err.println("解密后:\t "+outputStr);
        
        //校验
        assertEquals(inputStr,outputStr);
        
    }
    
    
}
