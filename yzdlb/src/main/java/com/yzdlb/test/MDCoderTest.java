package com.yzdlb.test;
import static org.testng.Assert.*;

import org.testng.annotations.Test;

import com.yzdlb.security.util.MDCoderUtil;

/***************************************************************************
 * <pre></pre>
 * @ClassName:  MDCodeTest.java
 * @PackagePath:  com.yzdlb.test
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:  
 * @Author: yangzhendong
 * @Date: 2016年5月31日 下午3:16:21
 ***************************************************************************/
public class MDCoderTest {
    
    @Test
    public final void testEncodeMD2() throws Exception{
        String str = "MD2消息摘要";
        byte[] data1 = MDCoderUtil.encodeMD2(str.getBytes());
        byte[] data2 = MDCoderUtil.encodeMD2(str.getBytes());
        assertEquals(data1, data2);
    }
    @Test
    public final void testEncodeMD5()throws Exception{
        String str = "MD5消息摘要";
        byte[] data1 = MDCoderUtil.encodeMD5(str.getBytes());
        byte[] data2 = MDCoderUtil.encodeMD5(str.getBytes());
        assertEquals(data1,data2);
    }
}
