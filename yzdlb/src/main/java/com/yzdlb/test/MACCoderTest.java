package com.yzdlb.test;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.yzdlb.security.util.MACCoder;
/***************************************************************************
 * <pre></pre>
 * @ClassName:  MACCoderTest.java
 * @PackagePath:  com.yzdlb.test
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:  
 * @Author: yangzhendong
 * @Date: 2016年6月3日 上午10:31:28
 ***************************************************************************/
public class MACCoderTest {
     
    @Test
    public final void testEncodeHmacSHA256() throws Exception{
        String str = "HmacSHA256消息摘要";
        byte[] key = MACCoder.initHmacSHA256Key();
        
        byte[] data1 = MACCoder.encodeHmacSHA256(str.getBytes(), key);
        byte[] data2 = MACCoder.encodeHmacSHA256(str.getBytes(), key);
        Assert.assertEquals(data1, data2);
    }
    @Test
    public final void testEncodeHmacMD5() throws Exception{
        String str = "HmacMD5消息摘要";
        byte[] key = MACCoder.initHmacMD5Key();
        
        byte[] data1 = MACCoder.encodeHmacMD5(str.getBytes(), key);
        byte[] data2 = MACCoder.encodeHmacMD5(str.getBytes(), key);
        Assert.assertEquals(data1, data2);
    }
}
