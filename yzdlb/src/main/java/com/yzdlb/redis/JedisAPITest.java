package com.yzdlb.redis;

import java.util.HashSet;
import java.util.Set;

import redis.clients.jedis.Jedis;

public class JedisAPITest {
	public static Jedis jedis;

	static {
		jedis = new Jedis("localhost");
		//密码授权
		jedis.auth("yangzhendong");
	}

	/**
	 * 如果已存在则追加， 如果不存在则新添加
	 */
	public static void append(){
		jedis.set("foo", "too");
		jedis.append("foo", "123");
		System.out.println(jedis.get("foo"));

		jedis.append("none", "123");
		System.out.println(jedis.get("none"));
	}

	public static void brpoplpush(){
		jedis.brpoplpush("", "", 0);
	}

	//集合Set操作
	public static void testSet(){
		jedis.sadd("testSet","帽子");
		jedis.sadd("testSet","衣服","裤子","鞋子");
		jedis.sadd("testSet","帽子");
		jedis.sadd("testSet","redis");
		System.out.println(jedis.smembers("testSet"));
		jedis.srem("testSet", "redis");
		System.out.println(jedis.smembers("testSet"));
		System.out.println(jedis.sismember("testSet", "帽子"));//判断 who 是否是user集合的元素  
		System.out.println(jedis.srandmember("testSet"));   //获取随机值
		System.out.println(jedis.scard("testSet"));//返回集合的元素个数

	}

	public static void testList(){
		jedis.del("testList");
		System.out.println(jedis.lrange("testList", 0, -1));
		jedis.lpush("testList","1");
		jedis.lpush("testList","2");
		jedis.lpush("testList","3");
		System.out.println(jedis.lrange("testList", 0, -1));
		
		System.out.println(jedis.lrange("testList", 0, 1));
		
		jedis.rpush("testList", "1");
		
	}


	public static void main(String[] args) {
		testSet();
	}
}
