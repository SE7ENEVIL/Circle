package com.yzdlb.redis;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import redis.clients.jedis.Jedis;

public class RedisJava {
	public static void main(String[] args) {
		Jedis jedis = new Jedis("localhost");
		System.out.println("Connection to server successfully");
		System.out.println("Server is running: " + jedis.ping());
		jedis.set("runoobkey", "Redis tutorial");
		System.out.println("Stored string in redis:: " + jedis.get("runoobkey"));
		
		jedis.lpush("tutorial-list", "Redis");
		jedis.lpush("tutorial-list", "Mongodb");
		jedis.lpush("tutorial-list", "Mysql");
		
		List<String> list = jedis.lrange("tutorial-list", 0, 5);
		for(int i=0;i<list.size();i++){
			System.out.println("Stored string in redis:: " + list.get(i));
		}
		
		Set<String> list1 =  jedis.keys("*");
		
		Iterator<String> it = list1.iterator();
		while(it.hasNext()){
			System.out.println("List of stored keys:: "+it.next());
		}
		
		jedis.set("foo", "too");
		jedis.append("foo", "123");
		System.out.println("");
		
	}
}
