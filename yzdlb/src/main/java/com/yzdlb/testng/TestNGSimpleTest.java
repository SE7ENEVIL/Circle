
package com.yzdlb.testng;

import org.testng.annotations.*;
import static org.testng.Assert.*;
/***************************************************************************
 * <pre></pre>
 * @ClassName:  TestNGSimpleTest.java
 * @PackagePath:  com.yzdlb.testng
 * @Copyright: 北京数字认证股份有限公司 (C) 2016
 * @Description:  
 * @Author: yangzhendong
 * @Date: 2016年7月1日 下午4:36:47
 ***************************************************************************/
public class TestNGSimpleTest {
    @Test
    public void testAdd(){
        
        String str = "TestNG";
        assertEquals("TestNG",str);
    }
}
