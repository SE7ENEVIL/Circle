package com.yzdlb.shejimoshi.decorator;

public class DecoratorTest {
	public static void main(String[] args) {
		IThirdParty a = new ThirdParty();
		IThirdParty d1 = new Decorator1(a);
		IThirdParty d2 = new Decorator2(d1);
		String s = d2.sayMsg();
		System.out.println(s);
	}
}
