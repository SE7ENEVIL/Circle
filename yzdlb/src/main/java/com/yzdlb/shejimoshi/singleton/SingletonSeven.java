package com.yzdlb.shejimoshi.singleton;

/**
 * 双重检查锁定  jdk1.5以上
 * @author Carl
 *
 */
public class SingletonSeven {
	private volatile static SingletonSeven singleton;
	private SingletonSeven(){}

	public static SingletonSeven getSingletonSeven(){
		if(singleton ==null){
			synchronized (SingletonSeven.class){
				if(singleton == null){
					singleton  =  new SingletonSeven();
				}
			}
		}
		return singleton;
	}
}
