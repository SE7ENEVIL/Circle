package com.yzdlb.shejimoshi.singleton;

/**
 * 枚举 jdk1.5以上 推荐使用
 * @author Carl
 *
 */
public enum SingletonSix {
	INSTANCE;
	public void whateverMethod(){
		System.out.println("处理内容");
	}
	
	public static void main(String[] args) {
		SingletonSix.INSTANCE.whateverMethod();
	}
}

