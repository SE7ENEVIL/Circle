package com.yzdlb.shejimoshi.singleton;


public class SingletonThree {
	private static SingletonThree instance = new SingletonThree();
	private SingletonThree(){}
	public static SingletonThree getInstance(){
		return instance;
	}
}
