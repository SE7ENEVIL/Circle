package com.yzdlb.shejimoshi.singleton;

/**
 * 线程不安全
 * @author Carl
 *
 */
public class SingletonOne {
	
	private static SingletonOne instance;
	//封闭构造方法
	private SingletonOne (){}
	
	public static SingletonOne getInstance(){
		if(instance == null){
			instance = new SingletonOne();
		}
		return instance;
	}
	
}
