package com.yzdlb.shejimoshi.singleton;

public class SingletonTwo {
	private static SingletonTwo instance;
	//封闭构造方法
	private SingletonTwo (){}
	
	//增加同步
	public static synchronized SingletonTwo getInstance(){
		if(instance == null){
			instance = new SingletonTwo();
		}
		return instance;
	}
}
