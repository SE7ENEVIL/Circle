package com.yzdlb.shejimoshi.singleton;

public class SingletonFour {
	private static SingletonFour instance = null;
	static{
		instance = new SingletonFour();
	}
	private SingletonFour(){}
	public static SingletonFour getInstance(){
		return instance;
	}
}
